describe('App', () => {
  it('increments the counter', () => {
    cy.visit('/')
    cy.get('#counterButton').click()
    cy.get('#counterButton').should(
        'contain',
        'count is: 1'
    )
  })
})
