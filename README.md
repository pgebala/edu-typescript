## React + Typescript EDU

Celem zadania było stworzenie projektu React z użyciem Typescript oraz poznanie narzędzi ułatwiających development.

### Wykorzystane technologie

- ReactJS
- Typescript
- Vite
- Eslint
- Stylelint
- Prettier
- Jest
- Cypress
- i18next
- mui
- 

