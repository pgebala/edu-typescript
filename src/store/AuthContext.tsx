import React, { useState, useEffect, useCallback } from 'react';

export interface TokenData {
  token: string;
  duration: number;
}

let logoutTimer: NodeJS.Timeout;

const AuthContext = React.createContext({
  token: '',
  isLoggedIn: false,
  login: (tokenString: string, expirationTime: string) => {
    // default
  },
  logout: () => {
    // default
  }
});

const calculateRemainingTime = (expirationTime: string): number => {
  const currentTime = new Date().getTime();
  const adjExpirationTime = new Date(expirationTime).getTime();

  return adjExpirationTime - currentTime;
};

const retrieveStoredToken = (): TokenData | null => {
  const storedToken: string = localStorage.getItem('token') || '';
  const storedExpirationDate: string =
    localStorage.getItem('expirationTime') || '';

  const remainingTime: number = calculateRemainingTime(storedExpirationDate);

  if (remainingTime <= 3600) {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationTime');
    return null;
  }

  return {
    token: storedToken,
    duration: remainingTime
  };
};

export const AuthContextProvider: React.FC<{ children?: React.ReactNode }> = (
  props
) => {
  const tokenData: TokenData | null = retrieveStoredToken();

  let initialToken = null;
  if (tokenData) {
    initialToken = tokenData.token;
  }

  const [token, setToken] = useState<string | null>(initialToken);

  const userIsLoggedIn = !!token;

  const logoutHandler = useCallback(() => {
    setToken(null);
    localStorage.removeItem('token');
    localStorage.removeItem('expirationTime');

    if (logoutTimer) {
      clearTimeout(logoutTimer);
    }
  }, []);

  const loginHandler = (tokenString: string, expirationTime: string) => {
    setToken(tokenString);
    localStorage.setItem('token', tokenString);
    localStorage.setItem('expirationTime', expirationTime);

    const remainingTime = calculateRemainingTime(expirationTime);

    logoutTimer = setTimeout(logoutHandler, remainingTime);
  };

  useEffect(() => {
    if (tokenData) {
      logoutTimer = setTimeout(logoutHandler, tokenData.duration);
    }
  }, [tokenData, logoutHandler]);

  const contextValue = {
    token: token || '',
    isLoggedIn: userIsLoggedIn,
    login: loginHandler,
    logout: logoutHandler
  };

  return (
    <AuthContext.Provider value={contextValue}>
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
