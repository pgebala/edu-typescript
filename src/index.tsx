import i18next from 'i18next';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { I18nextProvider } from 'react-i18next';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import './index.css';
import { AuthContextProvider } from './store/AuthContext';
import common_en from './translations/en/common.json';
import common_pl from './translations/pl/common.json';

void i18next.init({
  interpolation: { escapeValue: false },
  lng: 'en',
  resources: {
    en: {
      common: common_en
    },
    pl: {
      common: common_pl
    }
  }
});

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <BrowserRouter>
    <I18nextProvider i18n={i18next}>
      <AuthContextProvider>
        <App />
      </AuthContextProvider>
    </I18nextProvider>
  </BrowserRouter>
);
