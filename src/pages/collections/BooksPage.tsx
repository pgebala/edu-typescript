import BookIcon from '@mui/icons-material/Book';
import {
  Avatar,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@mui/material';

const BooksPage = () => {
  return (
    <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <BookIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Books collection" secondary="Sep 5, 2022" />
      </ListItem>
    </List>
  );
};

export default BooksPage;
