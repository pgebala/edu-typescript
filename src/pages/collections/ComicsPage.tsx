import StyleIcon from '@mui/icons-material/Style';
import {
  Avatar,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@mui/material';

const ComicsPage = () => {
  return (
    <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <StyleIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Comics collection" secondary="Sep 5, 2022" />
      </ListItem>
    </List>
  );
};

export default ComicsPage;
