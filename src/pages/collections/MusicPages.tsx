import MusicVideoIcon from '@mui/icons-material/MusicVideo';
import {
  Avatar,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@mui/material';

const MusicPage = () => {
  return (
    <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <MusicVideoIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Music collection" secondary="Sep 5, 2022" />
      </ListItem>
    </List>
  );
};

export default MusicPage;
