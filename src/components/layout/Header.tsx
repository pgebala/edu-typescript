import { AppBar, Button, IconButton, Toolbar, Typography } from '@mui/material';
import * as React from 'react';
import { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import logo from '../../assets/react.svg';
import AuthContext from '../../store/AuthContext';
import LanguageSelect from './LanguageSelect';

const Header = () => {
  const { t } = useTranslation('common');
  const authCtx = useContext(AuthContext);

  const handleClick = () => {
    authCtx.logout();
  };
  return (
    <AppBar
      position="fixed"
      sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}
    >
      <Toolbar>
        <IconButton
          size="large"
          edge="start"
          color="inherit"
          aria-label="menu"
          sx={{ mr: 2 }}
        >
          <header>
            <img src={logo} alt="React Logo" height="40" />
          </header>
        </IconButton>
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          Collector
        </Typography>
        <LanguageSelect />
        <Button
          onClick={handleClick}
          sx={{ m: 1, minWidth: 120 }}
          color="inherit"
        >
          {t('header.signOut')}
        </Button>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
