import { Box, CssBaseline, Drawer, Toolbar } from '@mui/material';
import React, { Fragment, useContext } from 'react';
import AuthContext from '../../store/AuthContext';
import Header from './Header';
import Navigation from './Navigation';

const drawerWidth = 240;

const Layout: React.FC<{ children?: React.ReactNode }> = (props) => {
  const authCtx = useContext(AuthContext);
  const isLoggedIn = authCtx.isLoggedIn;
  return (
    <Fragment>
      {isLoggedIn && (
        <Box sx={{ display: 'flex' }}>
          <CssBaseline />
          <Header />
          <Drawer
            variant="permanent"
            sx={{
              width: drawerWidth,
              flexShrink: 0,
              [`& .MuiDrawer-paper`]: {
                width: drawerWidth,
                boxSizing: 'border-box'
              }
            }}
          >
            <Toolbar />
            <Box sx={{ overflow: 'auto' }}>
              <Navigation />
            </Box>
          </Drawer>
          <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
            <Toolbar />
            <main>{props.children}</main>
          </Box>
        </Box>
      )}
      {!isLoggedIn && <main>{props.children}</main>}
    </Fragment>
  );
};

export default Layout;
