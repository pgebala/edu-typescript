import { ExpandLess, ExpandMore } from '@mui/icons-material';
import BookIcon from '@mui/icons-material/Book';
import HomeIcon from '@mui/icons-material/Home';
import LibraryBooksIcon from '@mui/icons-material/LibraryBooks';
import MusicVideoIcon from '@mui/icons-material/MusicVideo';
import StyleIcon from '@mui/icons-material/Style';
import {
  Collapse,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

const Navigation = () => {
  const { t } = useTranslation('common');

  const [open, setOpen] = React.useState(false);
  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <List
      sx={{ pt: 4 }}
      component="nav"
      aria-labelledby="nested-list-subheader"
    >
      <ListItemButton component={Link} to={'/'}>
        <ListItemIcon>
          <HomeIcon />
        </ListItemIcon>
        <ListItemText primary={t('navigation.home')} />
      </ListItemButton>
      <ListItemButton onClick={handleClick}>
        <ListItemIcon>
          <LibraryBooksIcon />
        </ListItemIcon>
        <ListItemText primary={t('navigation.collections')} />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItemButton component={Link} to={'/books'} sx={{ pl: 4 }}>
            <ListItemIcon>
              <BookIcon />
            </ListItemIcon>
            <ListItemText primary={t('navigation.books')} />
          </ListItemButton>
          <ListItemButton component={Link} to={'/comics'} sx={{ pl: 4 }}>
            <ListItemIcon>
              <StyleIcon />
            </ListItemIcon>
            <ListItemText primary={t('navigation.comics')} />
          </ListItemButton>
          <ListItemButton component={Link} to={'/music'} sx={{ pl: 4 }}>
            <ListItemIcon>
              <MusicVideoIcon />
            </ListItemIcon>
            <ListItemText primary={t('navigation.music')} />
          </ListItemButton>
        </List>
      </Collapse>
    </List>
  );
};

export default Navigation;
