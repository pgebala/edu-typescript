import {
  FormControl,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';
import styles from './LanguageSelect.module.css';

const LanguageSelect = () => {
  const { i18n } = useTranslation('common');

  const handleChange = (event: SelectChangeEvent) => {
    void i18n.changeLanguage(event.target.value);
  };

  return (
    <FormControl sx={{ m: 1, minWidth: 60 }} size="small">
      <Select
        id="lang-select-small"
        className={styles.select}
        defaultValue={'en'}
        onChange={handleChange}
      >
        <MenuItem value={'en'}>EN</MenuItem>
        <MenuItem value={'pl'}>PL</MenuItem>
      </Select>
    </FormControl>
  );
};

export default LanguageSelect;
