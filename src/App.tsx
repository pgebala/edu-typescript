import { useContext } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import ProtectedRoute, {
  ProtectedRouteProps
} from './components/auth/ProtectedRoute';
import Layout from './components/layout/Layout';
import AuthPage from './pages/AuthPage';
import BooksPage from './pages/collections/BooksPage';
import ComicsPage from './pages/collections/ComicsPage';
import MusicPage from './pages/collections/MusicPages';
import HomePage from './pages/HomePage';
import AuthContext from './store/AuthContext';

const defaultProtectedRouteProps: Omit<ProtectedRouteProps, 'outlet'> = {
  isAuthenticated: false,
  authenticationPath: '/auth'
};

function App() {
  const authCtx = useContext(AuthContext);
  defaultProtectedRouteProps.isAuthenticated = authCtx.isLoggedIn;

  return (
    <Layout>
      <Routes>
        {!authCtx.isLoggedIn && <Route path="/auth" element={<AuthPage />} />}
        <Route
          path="/"
          element={
            <ProtectedRoute
              {...defaultProtectedRouteProps}
              outlet={<HomePage />}
            />
          }
        />
        <Route
          path="/books"
          element={
            <ProtectedRoute
              {...defaultProtectedRouteProps}
              outlet={<BooksPage />}
            />
          }
        />
        <Route
          path="/comics"
          element={
            <ProtectedRoute
              {...defaultProtectedRouteProps}
              outlet={<ComicsPage />}
            />
          }
        />
        <Route
          path="/music"
          element={
            <ProtectedRoute
              {...defaultProtectedRouteProps}
              outlet={<MusicPage />}
            />
          }
        />
        <Route path="*" element={<Navigate to="/" replace />} />
      </Routes>
    </Layout>
  );
}

export default App;
